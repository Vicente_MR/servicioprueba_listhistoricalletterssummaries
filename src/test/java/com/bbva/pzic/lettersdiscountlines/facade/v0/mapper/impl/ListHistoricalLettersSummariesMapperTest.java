package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.lettersdiscountlines.EntityMock;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ListHistoricalLettersSummariesMapperTest {

    private ListHistoricalLettersSummariesMapper mapper = new ListHistoricalLettersSummariesMapper();

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntHistoricalLettersSummariesId result = mapper.mapIn(EntityMock.BUSINESS_ID);
        assertNotNull(result);
        assertNotNull(result.getBusinessId());

        assertEquals(EntityMock.BUSINESS_ID, result.getBusinessId());
    }

    @Test
    public void mapInEmptyTest() throws IOException {
        DTOIntHistoricalLettersSummariesId result = mapper.mapIn(null);

        assertNotNull(result);
        assertNull(result.getBusinessId());
    }

    @Test
    public void mapOutFullTest() throws IOException {
        ServiceResponse<List<HistoricalLettersSummaries>> result = mapper.mapOut(Collections.singletonList(new HistoricalLettersSummaries()));

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void mapOutEmptyTest() throws IOException {
        ServiceResponse<List<HistoricalLettersSummaries>> result = mapper.mapOut(Collections.emptyList());

        assertNull(result);
    }
}
