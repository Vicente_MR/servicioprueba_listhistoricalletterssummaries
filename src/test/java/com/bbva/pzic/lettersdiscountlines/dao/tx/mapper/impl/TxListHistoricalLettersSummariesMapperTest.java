package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl;

import com.bbva.pzic.lettersdiscountlines.EntityMock;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.mock.FormatoRif4Stubs;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TxListHistoricalLettersSummariesMapperTest {

    private EntityMock entityMock = EntityMock.getInstance();

    @InjectMocks
    private TxListHistoricalLettersSummariesMapper mapper;

    private FormatoRif4Stubs formatoRif4Stubs = FormatoRif4Stubs.getInstance();

    @Test
    public void mapInFullTest() throws IOException {
        DTOIntHistoricalLettersSummariesId input = entityMock.getDTOIntHistoricalLettersSummariesId();
        FormatoRIMRF40 result = mapper.mapIn(input);

        assertNotNull(result);
    }

    @Test
    public void mapInEmptyTest() throws IOException{
        DTOIntHistoricalLettersSummariesId input = new DTOIntHistoricalLettersSummariesId();
        FormatoRIMRF40 result = mapper.mapIn(input);

        assertNotNull(result);
        assertNull(result.getCodcent());
    }

    @Test
    public void mapOutFullTest() throws IOException{
        FormatoRIMRF41 input = formatoRif4Stubs.getFormatoRIMRF41();
        List<HistoricalLettersSummaries> historicalLettersSummaries = new ArrayList<>();
        historicalLettersSummaries = mapper.mapOut(input, historicalLettersSummaries);

        HistoricalLettersSummaries result = historicalLettersSummaries.get(0);

        assertNotNull(result);
        assertNotNull(result.getId());
        assertNotNull(result.getName());

        assertEquals(input.getId(), result.getId());
        assertEquals(input.getEfedes(), result.getName());
    }

    @Test
    public void mapOut2FullTest() throws IOException{
        FormatoRIMRF42 input = formatoRif4Stubs.getFormatoRIMRF42();
        List<HistoricalLettersSummaries> historicalLettersSummaries = new ArrayList<>();
        historicalLettersSummaries.add(new HistoricalLettersSummaries());

        historicalLettersSummaries = mapper.mapOut2(input, historicalLettersSummaries);
        HistoricalLettersSummaries result = historicalLettersSummaries.get(0);

        assertNotNull(result);
        assertNotNull(result.getHistoricalParameters());
        assertNotNull(result.getHistoricalParameters().get(0).getPercentage());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertNotNull(result.getHistoricalParameters().get(0).getInformationPeriod().getUnit());

        assertEquals(input.getPerefel(), result.getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertEquals(input.getPorefe(), result.getHistoricalParameters().get(0).getPercentage());
        assertEquals(input.getUniefel(), result.getHistoricalParameters().get(0).getInformationPeriod().getUnit());
    }
}
