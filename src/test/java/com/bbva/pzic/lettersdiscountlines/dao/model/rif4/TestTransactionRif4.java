package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestTransactionRif4 {

    @InjectMocks
    private TransaccionRif4 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion{

        PeticionTransaccionRif4 peticion = new PeticionTransaccionRif4();
        RespuestaTransaccionRif4 respuesta = new RespuestaTransaccionRif4();

        Mockito.when(servicioTransacciones.invocar(PeticionTransaccionRif4.class, RespuestaTransaccionRif4.class, peticion)).thenReturn(respuesta);

        RespuestaTransaccionRif4 result = transaccion.invocar(peticion);

        Assert.assertEquals(result, respuesta);
    }
}
