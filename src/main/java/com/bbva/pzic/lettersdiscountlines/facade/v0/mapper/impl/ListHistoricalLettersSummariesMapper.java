package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl;


import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.IListHistoricalLettersSummariesMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

@Component
public class ListHistoricalLettersSummariesMapper implements IListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(ListHistoricalLettersSummariesMapper.class);


    @Override
    public DTOIntHistoricalLettersSummariesId mapIn(final String businessId) {
        LOG.info("... called method ListHistoricalLettersSummariesMapper.mapIn ...");
        DTOIntHistoricalLettersSummariesId dtoInt = new DTOIntHistoricalLettersSummariesId();
        dtoInt.setBusinessId(businessId);
        return dtoInt;
    }

    @Override
    public ServiceResponse<List<HistoricalLettersSummaries>> mapOut(final List<HistoricalLettersSummaries> input) {
        LOG.info("... called method ListHistoricalLettersSummariesMapper.mapOut ...");
        if(CollectionUtils.isEmpty(input)){
            return null;
        }
        return ServiceResponse.data(input).build();
    }
}
