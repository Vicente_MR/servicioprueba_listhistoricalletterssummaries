package com.bbva.pzic.lettersdiscountlines.facade.v0.impl;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;
import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.lettersdiscountlines.business.ISrvIntLettersDiscountLines;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.IListHistoricalLettersSummariesMapper;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import com.bbva.pzic.lettersdiscountlines.facade.v0.ISrvLettersDiscountLinesV0;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bbva.pzic.lettersdiscountlines.facade.RegistryIds.*;

/**
 * Created on 23/12/2020.
 *
 * @author Entelgy
 */
@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SMGG20203815", logicalID = "listHistoricalLettersSummaries")
@VN(vnn = "v0")
@Service
public class SrvLettersDiscountLinesV0 implements ISrvLettersDiscountLinesV0 {

    private static final Log LOG = LogFactory.getLog(SrvLettersDiscountLinesV0.class);

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;
    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;

    @Autowired
    private IListHistoricalLettersSummariesMapper listHistoricalLettersSummariesMapper;

    @Autowired
    private ISrvIntLettersDiscountLines srvIntLettersDiscountLines;

    @Override
    @GET
    @Path("/historical-letters-summaries")
    @SMC(registryID = SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES, logicalID = "listHistoricalLettersSummaries")
    public ServiceResponse<List<HistoricalLettersSummaries>> listHistoricalLettersSummaries(@QueryParam("businessId") final String businessId) {
        LOG.info("----- Invoking service listHistoricalLettersSummaries -----");

        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("businessId", businessId);

        inputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES, null, null, queryParams);
        ServiceResponse<List<HistoricalLettersSummaries>> serviceResponse = listHistoricalLettersSummariesMapper.mapOut(
                srvIntLettersDiscountLines.listHistoricalLettersSummaries(
                    listHistoricalLettersSummariesMapper.mapIn((String) queryParams.get("businessId"))
                )
        );

        outputDataProcessingExecutor.perform(SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES, serviceResponse, null, null);
        return serviceResponse;
    }
}
