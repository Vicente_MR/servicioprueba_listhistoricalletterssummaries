package com.bbva.pzic.lettersdiscountlines.facade;

/**
 * @author Entelgy
 */
public class RegistryIds {
    public static final String SMC_REGISTRY_ID_OF_LIST_HISTORICAL_LETTERS_SUMMARIES = "SMGG20203815";

    private RegistryIds(){
    }
}
