package com.bbva.pzic.lettersdiscountlines.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "historicalLettersSummaries", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlType(name = "historicalLettersSummaries", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoricalLettersSummaries {

    private static final long serialVersionUID = 1L;

    /**
     * Identifier associated with the historical summary.
     */
    private String id;
    /**
     * Name associated with the historical summary.
     */
    private String name;
    /**
     * List of values associated with the historical summary.
     */
    private List<HistoricalParameters> historicalParameters;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HistoricalParameters> getHistoricalParameters() {
        return historicalParameters;
    }

    public void setHistoricalParameters(List<HistoricalParameters> historicalParameters) {
        this.historicalParameters = historicalParameters;
    }
}
