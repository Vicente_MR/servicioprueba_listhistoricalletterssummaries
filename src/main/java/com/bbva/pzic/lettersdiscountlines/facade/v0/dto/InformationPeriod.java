package com.bbva.pzic.lettersdiscountlines.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "informationPeriod", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlType(name = "informationPeriod", namespace = "urn:com:bbva:pzic:lettersdiscountlines:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InformationPeriod {

    private static final long serialVersionUID = 1L;

    /**
     * Value associated with the information period, where it is represented a period of time.
     */
    private Integer number;
    /**
     * Value associated with the information period, where it is represented a unit of time.
     */
    private String unit;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
