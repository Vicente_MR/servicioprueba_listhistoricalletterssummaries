package com.bbva.pzic.lettersdiscountlines.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;

import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import java.util.List;

/**
 * Created on 23/12/2020.
 *
 * @author Entelgy
 */
public interface ISrvLettersDiscountLinesV0 {
    ServiceResponse<List<HistoricalLettersSummaries>> listHistoricalLettersSummaries(String businessID);
}
