package com.bbva.pzic.lettersdiscountlines.business;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import java.util.List;

public interface ISrvIntLettersDiscountLines {

    List<HistoricalLettersSummaries> listHistoricalLettersSummaries (DTOIntHistoricalLettersSummariesId input);
}
