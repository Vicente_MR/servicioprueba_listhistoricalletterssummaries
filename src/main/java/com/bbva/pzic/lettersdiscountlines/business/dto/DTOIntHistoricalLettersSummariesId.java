package com.bbva.pzic.lettersdiscountlines.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DTOIntHistoricalLettersSummariesId{

    @NotNull(groups = ValidationGroup.HistoricalLettersSummariesId.class)
    @Size(max = 8, groups = ValidationGroup.HistoricalLettersSummariesId.class)
    private String businessId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
