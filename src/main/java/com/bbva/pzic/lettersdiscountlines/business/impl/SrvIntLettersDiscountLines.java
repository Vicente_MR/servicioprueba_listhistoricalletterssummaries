package com.bbva.pzic.lettersdiscountlines.business.impl;

import com.bbva.pzic.lettersdiscountlines.business.ISrvIntLettersDiscountLines;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.business.dto.ValidationGroup;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SrvIntLettersDiscountLines implements ISrvIntLettersDiscountLines {
    private static final Log LOG = LogFactory.getLog(SrvIntLettersDiscountLines.class);

    @Autowired
    private Validator validator;

    @Override
    public List<HistoricalLettersSummaries> listHistoricalLettersSummaries(final DTOIntHistoricalLettersSummariesId input) {
        LOG.info("... Invoking method SrvIntLettersDiscountLines.listHistoricalLettersSummaries ...");
        LOG.info("... Validating listHistoricalLettersSumaries input parameter ...");
        validator.validate(input, ValidationGroup.HistoricalLettersSummariesId.class);
        return null;
    }
}
