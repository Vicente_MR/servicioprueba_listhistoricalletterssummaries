package com.bbva.pzic.lettersdiscountlines.dao;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

/**
 * Created on 23/12/2020.
 *
 * @author Entelgy
 */
public interface ILettersDiscountLinesDAO {
    List<HistoricalLettersSummaries> listHistoricalLettersSummaries (DTOIntHistoricalLettersSummariesId input);
}
