package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;

import java.util.List;

public interface ItxListHistoricalLettersSummariesMapper {

    FormatoRIMRF40 mapIn(DTOIntHistoricalLettersSummariesId dtoIn);

    List<HistoricalLettersSummaries> mapOut(FormatoRIMRF41 formatOutput, List<HistoricalLettersSummaries> historicalLettersSummaries);

    List<HistoricalLettersSummaries> mapOut2(FormatoRIMRF42 formatOutput, List<HistoricalLettersSummaries> historicalLettersSummaries);
}
