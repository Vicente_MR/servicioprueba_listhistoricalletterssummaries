package com.bbva.pzic.lettersdiscountlines.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ItxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created on 28/12/2020.
 *
 * @author Entelgy
 */

@Component
public class TxListHistoricalLettersSummaries extends DoubleOutputFormat<DTOIntHistoricalLettersSummariesId, FormatoRIMRF40, List<HistoricalLettersSummaries>, FormatoRIMRF41, FormatoRIMRF42>{

    @Resource(name = "TxListHistoricalLettersSummariesMapper")
    private ItxListHistoricalLettersSummariesMapper mapper;

    public TxListHistoricalLettersSummaries(InvocadorTransaccion transaction, Supplier<MensajeMultiparte> requestInstance, Supplier<List<HistoricalLettersSummaries>> outputInstance, Class<FormatoRIMRF41> firstFormatClass, Class<FormatoRIMRF42> secondFormatClass) {
        super(transaction, requestInstance, outputInstance, firstFormatClass, secondFormatClass);
    }

    @Override
    protected FormatoRIMRF40 mapInput(DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId) {
        return mapper.mapIn(dtoIntHistoricalLettersSummariesId);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapFirstOutputFormat(FormatoRIMRF41 formatoRIMRF41, DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        return mapper.mapOut(formatoRIMRF41, historicalLettersSummaries);
    }

    @Override
    protected List<HistoricalLettersSummaries> mapSecondOutputFormat(FormatoRIMRF42 formatoRIMRF42, DTOIntHistoricalLettersSummariesId dtoIntHistoricalLettersSummariesId, List<HistoricalLettersSummaries> historicalLettersSummaries) {
        return mapper.mapOut2(formatoRIMRF42, historicalLettersSummaries);
    }
}
