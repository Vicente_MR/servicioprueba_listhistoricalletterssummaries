package com.bbva.pzic.lettersdiscountlines.dao.model.rif4.mock;

import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public class FormatoRif4Stubs {

    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private static final FormatoRif4Stubs INSTANCE = new FormatoRif4Stubs();

    private FormatoRif4Stubs(){
    }

    public static FormatoRif4Stubs getInstance(){
        return INSTANCE;
    }

    public FormatoRIMRF41 getFormatoRIMRF41() throws IOException{
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com.bbva.pzic.lettersdiscountlines.dao.model.rift4.mock/formatoRIMRF41.json"), FormatoRIMRF41.class);
    }

    public FormatoRIMRF42 getFormatoRIMRF42() throws IOException{
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader().
                getResourceAsStream("com.bbva.pzic.lettersdiscountlines.dao.model.rift4.mock/formatoRIMRF42.json"), FormatoRIMRF42.class);
    }
}
