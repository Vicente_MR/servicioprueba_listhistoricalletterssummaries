package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;


import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>RIMRF40</code> de la transacci&oacute;n <code>RIF4</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "RIMRF40")
@RooJavaBean
@RooSerializable
public class FormatoRIMRF40 {

	/**
	 * <p>Campo <code>CODCENT</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CODCENT", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 8, longitudMaxima = 8)
	private String codcent;
	
}