package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ItxListHistoricalLettersSummariesMapper;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalParameters;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.InformationPeriod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class TxListHistoricalLettersSummariesMapper implements ItxListHistoricalLettersSummariesMapper {

    private static final Log LOG = LogFactory.getLog(TxListHistoricalLettersSummariesMapper.class);

    @Override
    public FormatoRIMRF40 mapIn(final DTOIntHistoricalLettersSummariesId input) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapIn ...");
        if(input == null){
            return null;
        }
        FormatoRIMRF40 dtoIn = new FormatoRIMRF40();
        dtoIn.setCodcent(input.getBusinessId());
        return dtoIn;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut(final FormatoRIMRF41 formatOutput, final List<HistoricalLettersSummaries> historicalLettersSummaries) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut ...");

        HistoricalLettersSummaries historicalLettersSummarie = new HistoricalLettersSummaries();
        historicalLettersSummarie.setId(formatOutput.getId());
        historicalLettersSummarie.setName(formatOutput.getEfedes());
        historicalLettersSummaries.add(historicalLettersSummarie);
        return historicalLettersSummaries;
    }

    @Override
    public List<HistoricalLettersSummaries> mapOut2(final FormatoRIMRF42 formatOutput, final List<HistoricalLettersSummaries> historicalLettersSummaries) {
        LOG.info("... called method TxListHistoricalLettersSummariesMapper.mapOut2 ...");
        if(CollectionUtils.isEmpty(historicalLettersSummaries)){
            return null;
        }

        HistoricalLettersSummaries historicalLettersSummarie = historicalLettersSummaries.get(historicalLettersSummaries.size() - 1);
        if(CollectionUtils.isEmpty(historicalLettersSummarie.getHistoricalParameters())){
            historicalLettersSummarie.setHistoricalParameters(new ArrayList<>());
        }

        HistoricalParameters historicalParameters = new HistoricalParameters();
        historicalParameters.setPercentage(formatOutput.getPorefe());
        historicalParameters.setInformationPeriod(mapOutInformationPeriod(formatOutput.getPerefel(), formatOutput.getUniefel()));
        historicalLettersSummarie.getHistoricalParameters().add(historicalParameters);
        return historicalLettersSummaries;
    }

    private InformationPeriod mapOutInformationPeriod(final Integer perefel, final String uniefel) {
        if(perefel == null && uniefel == null){
            return null;
        }

        InformationPeriod informationPeriod = new InformationPeriod();
        informationPeriod.setNumber(perefel);
        informationPeriod.setUnit(uniefel);
        return informationPeriod;
    }
}