package com.bbva.pzic.lettersdiscountlines.dao.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOIntHistoricalLettersSummariesId;
import com.bbva.pzic.lettersdiscountlines.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummaries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LettersDiscountLinesDAO implements ILettersDiscountLinesDAO {

    private static final Log LOG = LogFactory.getLog(LettersDiscountLinesDAO.class);

    @Override
    public List<HistoricalLettersSummaries> listHistoricalLettersSummaries(DTOIntHistoricalLettersSummariesId input) {
        return null;
    }
}
